#Listagem de Informações do cliente
Este projeto utiliza as tecnologias, Jquery, SASS e GRUNT para criar uma estrutura de listagem de informações do cliente.

São três páginas para acessar as informações do cliente:

customer_orders:
Página para acessar os pedidos feitos pelo cliente

customer_adress:
Página para acessar os endereços dos clientes

customer_profile:
Página para acessar os perfis dos clientes

O código fonte dos arquivos estáticos estão dentro da pasta htdocs_src, depois eles são minificados e esfumaçados pelo grunt, e surgem na pasta htdocs, que será acessada pela aplicação.