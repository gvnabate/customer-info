module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// variável com declaração dos diretórios utilizados
		dirs: {
			ROOT				: '../',
			ROOT_HTTP			: '../htdocs',
			GENERATED_CSS		: '../htdocs/css',
			GENERATED_JS		: '../htdocs/js',
			GENERATED_IMAGES	: '../htdocs/images',
			GENERATED_FONTS		: '../htdocs/fonts',
			SRC_COFFEE			: 'js/coffee',
			SRC_SCSS			: 'scss',
			SRC_JS				: 'js',
			SRC_IMAGES			: 'images',
			SRC_FONTS			: 'fonts'
		},
		// limpar todas os arquivos das pastas : TODO: precisa revisar esta regra
		clean: {
			build: {
				src: [
					'<%= dirs.GENERATED_IMAGES %>/**/*',
					'<%= dirs.GENERATED_CSS %>/**/*.css',
					'<%= dirs.GENERATED_JS %>/**/*.js',
					'<%= dirs.GENERATED_FONTS %>/**/*'
				]
			}
		},
		// subtituir variáveis dos arquivos quando requisitado o modo de compilação especifico
		replace: {
			main: {
				src: ['<%= dirs.GENERATED_CSS %>/**/*.css'],
				overwrite: true,
				replacements: [
					{
						from: '../../htdocs_src/images',
						to: "../images"
					}
				]
			}
		},

		coffee: {
			compile: {
				options: { sourceMap: false },
				expand: true,
				flatten: true,
				cwd: '<%= dirs.SRC_COFFEE %>',
				src: ['**/*.coffee'],
				dest: '<%= dirs.GENERATED_JS %>',
				ext: '.js'
			}
		},

		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				eqnull: true,
				browser: true,
				indent: 4,
				undef: true,
				unused: true,
				evil: true,
				devel:true,
				globals: {
					"$": false,
    				"jQuery": false
				},
				ignores: [	/*'<%= dirs.GENERATED_JS %>/libs/*.js',
							'<%= dirs.GENERATED_JS %>/vendor/*.js'*/
							'<%= dirs.GENERATED_JS %>/vendors/**/*.js',
							'<%= dirs.GENERATED_JS %>/vendors.*.js'
						]
			},
			uses_defaults: ['<%= dirs.GENERATED_JS %>/*.js'],
		},

		compass: {
			scss: {
                options: {
                    //sourcemap: true
                }
            },
			compile: {
				options: {
					httpPath: '<%= dirs.ROOT_HTTP %>',
					cssDir:'<%= dirs.GENERATED_CSS %>',
					sassDir: '<%= dirs.SRC_SCSS %>',
					imagesDir: '<%= dirs.SRC_IMAGES %>',
					outputStyle: 'expanded',
					relativeAssets: true,
					raw: 'preferred_syntax = :scss\n',
					force: true,
					time : true
				},
			},
			deploy: {
				options: {
					httpPath: '<%= dirs.ROOT_HTTP %>',
					cssDir:'<%= dirs.GENERATED_CSS %>',
					sassDir: '<%= dirs.SRC_SCSS %>',
					imagesDir: '<%= dirs.SRC_IMAGES %>',
					outputStyle: 'compressed',
					relativeAssets: true,
					raw: 'preferred_syntax = :scss\n',
					force: true
				},
			}
		},

		csslint: {
			strict: {
				options: {
					'import': 2,
					'empty-rules': 2,
					'duplicate-properties': 2,
					'known-properties': 2,
					'vendor-prefix': 2,
					'star-property-hack': 2,
					'underscore-property-hack': 2,
					'universal-selector': 2,
					'zero-units': 2,
					'shorthand': 2
				},
				src: ['<%= dirs.GENERATED_CSS %>/*.css']
			}
		},

		concat: {
			options: { separator: ';' },
			vendors: { // arquivos de terceiros (não desenvolvidos pela equipe infrashop)
				src: ['<%= dirs.SRC_JS %>/vendors/concat/*.js'],
				dest: '<%= dirs.GENERATED_JS %>/vendors/vendors.others.js',
			}
		},

		imagemin: {
			main : {
				options:{
					pngquant: false
				},
				files: [{
					expand: true,
					cwd: '<%= dirs.SRC_IMAGES %>/',
					src: ['**/*.{png,jpg,jpeg,gif}'],
					dest: '<%= dirs.GENERATED_IMAGES %>/'
				}]
			}
		},
		copy: {
			dynamic_mappings: {
				files: [
					// includes files within path
					{
						expand: true,
						cwd: '<%= dirs.SRC_JS %>',
						src: ['**/*.js', '!vendors/concat/*'],
						dest: '<%= dirs.GENERATED_JS %>',
						// filter: function(filepath) {
      //   					return (grunt.file.isDir(filepath) && require('fs').readdirSync(filepath).length === 0);
      //   				}
					},
					{
						expand: true,
						cwd: '<%= dirs.SRC_FONTS %>',
						src: ['**/*'],
						dest: '<%= dirs.GENERATED_FONTS %>'
					},
					{
						expand: true,
						cwd: '<%= dirs.SRC_SCSS %>',
						src: ['**/*.css'],
						dest: '<%= dirs.GENERATED_CSS %>'
					},
				]
			}
		},

		uglify: {
			options : {
                beautify: false,
                report: "min",
                mangle: {
                    except: ['jQuery']
                },
                compress: {
                    global_defs: {
                      "DEBUG": false
                    }/*,
                    dead_code: true*/
                }
			},
			target: {
				cwd: '<%= dirs.GENERATED_JS %>',
				src: ['**/*.js'],
				dest: '<%= dirs.GENERATED_JS %>',
				expand: true
			},
		},

		clean: ['<%= dirs.GENERATED_JS %>/*.min.js'],

		watch: {
			options:{
				livereload: false,
				nospawn: true
			},
			compass: {
				files: ['<%= dirs.SRC_SCSS %>/**/*.scss'],
				tasks: ['compass:compile', 'replace'],
			},
			csslint: {
				files: ['<%= dirs.SRC_SCSS %>/*.css'],
				tasks: ['csslint'],
			},
			// coffee: {
			// 	files: ['<%= dirs.SRC_COFFEE %>/**/*.coffee'],
			// 	tasks: ['coffee'],
			// },
			concat: {
				files: ['<%= dirs.SRC_JS %>/vendors/concat/*.js'],
				tasks: ['concat']
			},
			copy: {
				files: ['<%= dirs.SRC_JS %>/**/*js', '!<%= dirs.SRC_JS %>/vendors/concat/*'],
				//tasks: ['copy', 'jshint']
				tasks: ['copy']
			},
			jshint: {
				files: ['<%= dirs.GENERATED_JS %>/*.js'],
				tasks: ['jshint'],
			},
			clean: {
				files: ['<%= dirs.GENERATED_JS %>/*.js'],
				tasks: ['clean'],
			},
			imagemin: {
				files: ['<%= dirs.SRC_IMAGES %>/**/*.{png,jpg,jpeg,gif}'],
				tasks: ['imagemin'],
			},
			uglify: {
				files: ['<%= dirs.GENERATED_JS %>/**/*.js'],
				tasks: ['uglify:target'],
			}
		},

		notify_hooks: {
			options: {
				enabled: true,
				max_jshint_notifications: 5,
				title: "<%= pkg.name %>"
			}
		},

		notify: {
			watch: {
				options: {
					title: 'Task Complete',
					message: 'SASS and Uglify finished running',
				}
			},
			deploy: {
				options: {
					title: 'Deploy pronto',
					message: 'Arquivos de deploy prontos!',
				}
			}
		},
	});

	//grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-csslint');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-notify');

	grunt.loadNpmTasks('grunt-devtools');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-text-replace');
	grunt.loadNpmTasks('grunt-contrib-clean');

	grunt.task.run('notify_hooks');

	grunt.registerTask('default', 'dev');
	grunt.registerTask('dev', [
								'clean',
								'compass',
								'copy',
								//'coffee',
								'concat',
								'replace',
								'imagemin'
							]);
	grunt.registerTask('deploy', [
								'clean',
								'compass:deploy',
								'copy',
								'imagemin',
								//'coffee',
								'concat',
								'uglify',
								'replace',
								'notify:deploy'
							]);
};
